# SQF Theme Pack

## Overview

This is theme pack with couple of my favourite theme merged with Armitxes SQF extension. "Splendid".

## Credits

Big thanks to all authors of themes that used in this pack. Contact me if you don`t like that your theme is in this pack.

- Armitxes
- ahmadawais
- asilverio (Abrahan Silverio)
- huacnlee (Jason Lee)
- jibjack (jaredgorski)
- miguelsolorio
- danharri
- Ruie Dela Peña
- ginfuru
